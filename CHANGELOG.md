## 0.6.2
- Change BpriverDebug._createFolder -> BpriverDebug._createFileSystem
    - folder に限定する必要が無い.
    - すべてに対応できる FileSystemModelRoster を引数に取る.

## 0.6.1
- Update bpriver_origin library
    - 0.6.1
- Update file_system_model library
    - 0.6.1

## 0.6.0
- Update bpriver_origin library
    - 0.6.0
- Update file_system_model library
    - 0.6.0
- Delete bpriver_yaml library

## 0.5.6
- Update bpriver yaml
    - 0.5.4
- Update file system model
    - 0.5.5

## 0.5.5
- Update bpriver origin
    - 0.5.2
- Update bpriver yaml
    - 0.5.3
- Update file system model
    - 0.5.4

## 0.5.4
- Delete BpriverDebug.createFileSystem
    - createFolder を使う.
- Create BpriverDebug.createFolder
    - 自動で suffix を追加する機能は削除.

## 0.5.3
- Update bpriver origin
    - 0.5.2
- Update bpriver yaml
    - 0.5.2
- Update file system model
    - 0.5.3

## 0.5.2
- Update bpriver origin
    - 0.5.1
- Update bpriver yaml
    - 0.5.1
- Update file system model
    - 0.5.2

## 0.5.1
- Update file system model
    - 0.5.1

## 0.5.0
- Update bpriver origin
    - 0.5.0
- Update bpriver yaml
    - 0.5.0
- Update file system model
    - 0.5.0

## 0.4.4
- Update bpriver origin
    - 0.4.2
- Update bpriver yaml
    - 0.4.2
- Update file system model
    - 0.4.4

## 0.4.3
- Update file system model
    - 0.4.3

## 0.4.2
- Update file system model
    - 0.4.2

## 0.4.1
- Update bpriver origin
    - 0.4.1
- Update bpriver yaml
    - 0.4.1
- Update file system model
    - 0.4.1

## 0.4.0
- Update bpriver origin1
    - 0.4.0
- Update bpriver yaml
    - 0.4.0
- Update file system model
    - 0.4.0

## 0.3.4
- Update bpriver origin
    - 0.3.4
- Update bpriver yaml
    - 0.3.4
- Update file system model
    - 0.3.4

## 0.3.3
- Update bpriver origin
    - 0.3.2
- Update file system model
    - 0.3.2

## 0.3.2
- Edit BpriverDebug._createFileSystem
    - folder の名前被りが発生しないように increment を suffix として追加する.
    - _createFileSystem('a', ...), _createFileSystem('a', ...), 
        - a_1, a_2, ..., といったように 番号を追加する.

## 0.3.1
- Update dart sdk
    - 3.6.0

## 1.0.0

- Initial version