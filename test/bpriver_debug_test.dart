// Copyright (C) 2023, the bpriver_debug project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_debug/bpriver_debug.dart';
import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:test/test.dart';
import 'package:path/path.dart' as p;

class AggregationPatternA
    with
        AggregationPattern
{
    final String valueA;
    final String valueB;
    final String valueC;
    AggregationPatternA(this.valueA, this.valueB, this.valueC);
    
    @override
    Map<String, Object> get properties {
        return {
            'valueA': valueA,
            'valueB': valueB,
            'valueC': valueC,
        };
    }

}

void main() async {

    final debug = BpriverDebug(testLocation: ['bpriver_debug_test']);

    // group('winMerge', () {
    //     test('expected: return  then a', () async {
    //         // 手動確認
    //         final base = debug;
    //         final result = await base.winMerge('abcde', 'xyzxx');
    //         final actual = result;
    //         final expected = Complete();
    //         expect(actual, expected);
    //     });
    //     test('expected: return  then b', () async {
    //         // 手動確認
    //         final left = AggregationPatternA('a', 'b', 'c');
    //         final right = AggregationPatternA('x', 'y', 'z');
    //         final base = debug;
    //         final result = await base.winMerge(left, right);
    //         final actual = result;
    //         final expected = Complete();
    //         expect(actual, expected);
    //     });
    // });

    // group('writeLoggerResult', () {
    //     test('expected: return Complete then ', () async {
    //         // 手動確認
    //         final historyA = History(
    //             Success<Object, Exception>,
    //             [],
    //             NotExist,
    //             'dummyFunctionA',
    //             {},
    //             {},
    //             [],
    //         );
    //         final historyB = History(
    //             Success<Object, Exception>,
    //             [],
    //             NotExist,
    //             'dummyFunctionB',
    //             {},
    //             {},
    //             [],
    //         );
    //         final log = Log(
    //             functionLocation: 'dummyFunctionC',
    //             historyList: [historyA, historyB],
    //         );
    //         final loggerResult = Success('wrapped', log);
    //         final base = debug;
    //         final result = await base.writeLoggerResult(loggerResult);
    //         final actual = result;
    //         final expected = Complete();
    //         expect(actual, expected);
    //     });
    // });

    group('createFileSystem', () {
        final unitTestLocation = 'create_file_system';
        final folder = FolderModel(unitTestLocation, FileSystemModelRoster([
            FileModel('x', 'xxx'),
        ]));
        test('expected: return String then', () async {
            final base = debug;
            final result = await base.createFileSystem(FileSystemModelRoster([folder]));
            final actual = result;
            final expected = Complete();
            expect(actual, expected);
            {
                final path = p.join(p.current, debug.testLocation, unitTestLocation);
                final fileSystemModel = (await FileSystemModel.fromPath(path)).asExpected.decode().asExpected;
                final expected = folder;
                expect(fileSystemModel, expected);
            }
        });
    });

    group('getPubspecVersion', () {
        const pubspec = 'pubspec.yaml';
        test('expected: return String then version の記述あり', () async {
            final unitTestFolderName = 'get_pub_spec_version_1';
            final unitTestFolderPath = p.join(debug.testLocation, unitTestFolderName);
            await debug.createFileSystem(FileSystemModelRoster([
                FolderModel(unitTestFolderName, FileSystemModelRoster([
                    FileModel(pubspec, 'name: a \nversion: 1.2.3'),
                ]))
            ]));
            final base = debug;
            final result = await base.getPubspecVersion(unitTestFolderPath);
            final actual = result;
            final expected = '1.2.3';
            expect(actual, expected);
        });
        test('expected: return String then version の記述無し', () async {
            final unitTestFolderName = 'get_pub_spec_version_2';
            final unitTestFolderPath = p.join(debug.testLocation, unitTestFolderName);
            await debug.createFileSystem(FileSystemModelRoster([
                FolderModel('get_pub_spec_version_2', FileSystemModelRoster([
                    FileModel(pubspec, 'name: a'),
                ]))
            ]));
            final base = debug;
            final result = await base.getPubspecVersion(unitTestFolderPath);
            final actual = result;
            final expected = '';
            expect(actual, expected);
        });
    });

}