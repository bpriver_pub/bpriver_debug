// Copyright (C) 2023, the bpriver_debug project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_debug/bpriver_debug.dart';

const _dartToolFolderName = '.dart_tool';
const _bpriverDebugFolderName = 'bpriver_debug';

// test code でのみ利用される前提なので
//  入力を楽にするために unsafe code を用意する.
//  unsafe code にして panic して止めないといけない.
//      result だと 結果を slot に格納して確認しないと exception を返しているかどうかわからない.
//  内部的には result で handling する.

/// {@template BpriverDebug}
/// 
/// {@endtemplate}
final class BpriverDebug
{

    final List<String> _testLocation;
    final String winMergePath;
    final int yamlIndentSize;

    BpriverDebug({
        List<String> testLocation = const [],
        this.winMergePath = 'C:\\Program Files\\WinMerge\\WinMergeU.exe',
        this.yamlIndentSize = 4,
    })
    :
        _testLocation = testLocation.isEmpty ? const ['default_location'] : testLocation
    {

        final log = Log(classLocation: BpriverDebug, functionLocation: '');

        // test location folder を空にする処理.
        final cleanUpInstanceCacheFolderResult = _cleanUpTestLocationFolder();
        log.add(cleanUpInstanceCacheFolderResult);
        if (cleanUpInstanceCacheFolderResult is! Success<Complete, BpriverDebugExceptionAB>) Result.panic(BpriverDebugErrorA(), log.monitor({'exception': cleanUpInstanceCacheFolderResult.wrapped}));

    }

    String get testLocation => p.joinAll([p.current, _dartToolFolderName, _bpriverDebugFolderName, ..._testLocation]);

    /// {@template BpriverDebug.getTestLocationContainedPath}
    /// return [testLocation] + [testLocationAndLater] path.
    /// {@endtemplate}
    String getTestLocationContainedPath(List<String> testLocationAndLater) => p.joinAll([testLocation, ...testLocationAndLater]);

    /// {@template BpriverDebug._getBpriverDebugFolder}
    /// .dart_tool 直下に bpriver_debug folder を配置し その [io.Directory] を返す.
    /// Package layout conventions | Dart: "https://dart.dev/tools/pub/package-layout#project-specific-caching-for-tools"
    /// {@endtemplate}
    Danger<io.Directory, BpriverDebugExceptionAB> _getBpriverDebugFolder() {

        final log = Log(classLocation: runtimeType, functionLocation: '_getBpriverDebugFolder');

        final currentFolderPath = p.current;
        final dartToolFolderPath = p.join(currentFolderPath, _dartToolFolderName);
        final dartToolFolderResult = FileSystem.fromPath(dartToolFolderPath);
        if (dartToolFolderResult.wrapped is! Folder) return Failure(BpriverDebugExceptionA(), log.monitor({'current folder path': currentFolderPath}));

        final bpriverDebugFolderPath = p.join(dartToolFolderPath, _bpriverDebugFolderName);
        final cacheFolder = io.Directory(bpriverDebugFolderPath);
        
        return Danger.tryAndConvert(
            () {
                if (cacheFolder.existsSync()) return cacheFolder;
                cacheFolder.createSync();
                return cacheFolder;
            },
            BpriverDebugException.fromFileSystemException,
            log,
        );

    }

    /// {@template BpriverDebug._getTestFolderPath}
    /// bpriver_debug folder 直下に [testLocation] folder を配置し その [io.Directory] を返す.
    /// {@endtemplate}
    Danger<io.Directory, BpriverDebugExceptionAB> _getTestFolderPath() {

        final log = Log(classLocation: runtimeType, functionLocation: '_getTestFolderPath');

        final getPackageCacheFolderResult = _getBpriverDebugFolder();
        log.add(getPackageCacheFolderResult);
        if (getPackageCacheFolderResult is! Success<io.Directory, BpriverDebugExceptionAB>) return Failure(getPackageCacheFolderResult.asException, log);
        final packageCacheFolder = getPackageCacheFolderResult.wrapped;

        final testLocationPath = p.joinAll([
            packageCacheFolder.path,
            ..._testLocation,
        ]);
        final instanceCacheFolder = io.Directory(testLocationPath);

        return Danger.tryAndConvert(
            () {
                if (instanceCacheFolder.existsSync()) return instanceCacheFolder;
                instanceCacheFolder.createSync(recursive: true);
                return instanceCacheFolder;
            },
            BpriverDebugException.fromFileSystemException,
            log,
        );

    }

    /// {@template BpriverDebug._cleanUpTestLocationFolder}
    /// Empty [testLocation] folder.
    /// {@endtemplate}
    Danger<Complete, BpriverDebugExceptionAB> _cleanUpTestLocationFolder() {

        final log = Log(classLocation: runtimeType, functionLocation: '_cleanUpTestLocationFolder');
        
        final getInstanceCacheFolderResult = _getTestFolderPath();
        log.add(getInstanceCacheFolderResult);
        if (getInstanceCacheFolderResult is! Success<io.Directory, BpriverDebugExceptionAB>) return Failure(getInstanceCacheFolderResult.asException, log);

        final instanceCacheFolder = getInstanceCacheFolderResult.wrapped;

        return Danger.tryAndConvert(
            () {
                if (instanceCacheFolder.existsSync()) {
                    instanceCacheFolder.deleteSync(recursive: true);
                }
                instanceCacheFolder.createSync(recursive: true);
                return Complete();
            },
            BpriverDebugException.fromFileSystemException,
            log,
        );

    }

    /// {@template BpriverDebug._winMerge}
    /// winmerge を起動し比較する.<br>
    /// 比較対象が DesignPatternToNestedStructureMapSignature を実装している場合 toNestedStructureMap() を yaml で比較する.<br>
    /// 比較対象が 上記以外の場合 toString() で比較する.<br>
    /// {@endtemplate}
    FutureDanger<Complete, BpriverDebugExceptionABE> _winMerge(Object actual, Object expected) async {

        const wlOption = '/wl';
        const wrOption = '/wr';

        final log = Log(classLocation: runtimeType, functionLocation: '_winMerge');

        final instanceCacheFolderResult = _getTestFolderPath();
        log.add(instanceCacheFolderResult);
        if (instanceCacheFolderResult is! Success<io.Directory, BpriverDebugExceptionAB>) return Failure(instanceCacheFolderResult.asException, log);
        final instanceCacheFolder = instanceCacheFolderResult.wrapped;

        final temporaryFolderPrefix = 'win_merge' + '_';
        final temporaryFolder = instanceCacheFolder.createTempSync(temporaryFolderPrefix);

        late final String leftFileContent;
        if (actual is DesignPatternToNestedStructureMapSignature) {
            final result = BpriverYaml.toYamlString(actual.toNestedStructureMap());
            log.add(result);
            if (result is! Success<String, BpriverYamlExceptionI>) return Failure(BpriverDebugException.fromBpriverYamlException(result.asException), log);
            leftFileContent = result.wrapped;
        } else {
            leftFileContent = actual.toString();
        }
        final leftFileLocation = temporaryFolder.path;
        final leftFileName = 'actual.txt';
        final leftFileFullPath = p.join(leftFileLocation, leftFileName);
        final leftFile = io.File(leftFileFullPath);

        late final String rightFileContent;
        if (expected is DesignPatternToNestedStructureMapSignature) {
            final result = BpriverYaml.toYamlString(expected.toNestedStructureMap());
            log.add(result);
            if (result is! Success<String, BpriverYamlExceptionI>) return Failure(BpriverDebugException.fromBpriverYamlException(result.asException), log);
            rightFileContent = result.wrapped;
        } else {
            rightFileContent = expected.toString();
        }
        final rightFileLocation = temporaryFolder.path;
        final rightFileName = 'expected.txt';
        final rightFileFullPath = p.join(rightFileLocation, rightFileName);
        final rightFile = io.File(rightFileFullPath);

        final settingResult = Danger.tryAndConvert(
            () {
                leftFile.createSync();
                leftFile.writeAsStringSync(leftFileContent);
                rightFile.createSync();
                rightFile.writeAsStringSync(rightFileContent);
                return Complete();
            },
            BpriverDebugException.fromFileSystemException,
            log,
        );
        if (settingResult is! Success<Complete, BpriverDebugExceptionB>) return Failure(settingResult.asException, log);
        
        await io.Process.start(
            winMergePath,
            [
                wlOption,
                leftFileFullPath,
                wrOption,
                rightFileFullPath,
            ],
        );

        return Success(Complete(), log);

    }

    // vscode から test を開始していると vscode では開けないっぽい.
    // なので console に path を表示して click すれば開けるようにする.
    /// {@template BpriverDebug._writeLoggerResult}
    /// Result instance を 整形した状態で .dart_tool/bpriver_debug/[_testLocation]/write_logger_result_xxx/log.yaml に書き出す.<br>
    /// {@endtemplate}
    FutureDanger<Complete, BpriverDebugExceptionABE> _writeLoggerResult(
        Result loggerResult,
        // {
        //     bool newWindowFlag = false,
        //     String? myVscodeFullPath,
        // }
    ) async {

        final log = Log(classLocation: runtimeType, functionLocation: '_writeLoggerResult');

        final instanceCacheFolderResult = _getTestFolderPath();
        log.add(instanceCacheFolderResult);
        if (instanceCacheFolderResult is! Success<io.Directory, BpriverDebugExceptionAB>) return Failure(instanceCacheFolderResult.asException, log);
        final instanceCacheFolder = instanceCacheFolderResult.wrapped;

        final temporaryFolderPrefix = 'write_logger_result' + '_';
        final temporaryFolder = instanceCacheFolder.createTempSync(temporaryFolderPrefix);

        final toYamlStringResult = BpriverYaml.toYamlString(loggerResult.toHistory().mold());
        log.add(toYamlStringResult);
        if (toYamlStringResult is! Success<String, BpriverYamlExceptionI>) return Failure(BpriverDebugException.fromBpriverYamlException(toYamlStringResult.asException), log);

        final logYamlFileContent = toYamlStringResult.wrapped;
        const logYamlFileName = 'log.yaml';
        final logYamlFile = io.File(p.join(temporaryFolder.path, logYamlFileName));


        // String vscodeFullPath = '';

        // How do I tell where the user's home directory is, in Dart? - Stack Overflow: "https://stackoverflow.com/questions/25498128/how-do-i-tell-where-the-users-home-directory-is-in-dart"
        // if (Platform.isWindows && myVscodeFullPath == null) {
        //     final appDatePath = Platform.environment['APPDATA']!;
        //     final appDateAfterPath = p.join('Local', 'Programs', 'Microsoft VS Code', 'Code.exe');
        //     vscodeFullPath = p.join(appDatePath, appDateAfterPath);
        //     // C:\Users\river\AppData\Roaming\ という path が取れるが おそらく古いと C:\Users\river\AppData\ に vscode はインストールされているため
        //     if (!io.File(vscodeFullPath).existsSync()) vscodeFullPath = p.join(p.dirname(appDatePath), appDateAfterPath);
        // } else if (myVscodeFullPath != null) {
        //     vscodeFullPath = myVscodeFullPath;
        // }

        final tryAndConvertResult = Danger.tryAndConvert(
            () {
                logYamlFile.createSync();
                logYamlFile.writeAsStringSync(logYamlFileContent);
                return Complete();
            },
            BpriverDebugException.fromFileSystemException,
            log,
        );
        if (tryAndConvertResult is! Success<Complete, BpriverDebugExceptionB>) return Failure(tryAndConvertResult.asException, log);

        // VSCode | コマンドプロンプトからファイルを開く: "https://www.javadrive.jp/vscode/file/index2.html"
        // final result = await io.Process.start(
        //     vscodeFullPath,
        //     // 'code',
        //     [
        //         '-n',
        //         // if (newWindowFlag) 'n',
        //         logYamlFile.path,
        //     ],
        // );

        // terminal から link を click して開く用.
        print('log yaml file location: ${logYamlFile.path}');

        return Success(Complete(), log); 

    }

    /// {@template BpriverDebug._createFileSystem}
    /// .dart_tool/bpriver_debug/[testLocation] を location として fileSystem.createAsComplement(location) を実行する.<br>
    /// {@endtemplate}
    FutureDanger<Complete, BpriverDebugExceptionABC> _createFileSystem(Iterable<FileSystemModel> fileSystems) async {

        final log = Log(classLocation: runtimeType, functionLocation: '_createFileSystem');

        final getTestFolderPathResult = _getTestFolderPath();
        log.add(getTestFolderPathResult);
        if (getTestFolderPathResult is! Success<io.Directory, BpriverDebugExceptionAB>) return Failure(getTestFolderPathResult.asException, log);
        final testLocationFolder = getTestFolderPathResult.wrapped;

        final fileSystemModelRoster = FileSystemModelRoster.result(fileSystems);
        log.add(fileSystemModelRoster);
        if (fileSystemModelRoster is! Success<FileSystemModelRoster<FileSystemModel>, RosterPatternExceptionA>) return Failure(BpriverDebugExceptionC(), log);

        final createAsComplementResult = await fileSystemModelRoster.wrapped.createAsComplement(testLocationFolder.path);
        log.add(createAsComplementResult);
        if (createAsComplementResult is! Success<Complete, io.FileSystemException>) return Failure(BpriverDebugExceptionB(), log.monitor({
            'exception': createAsComplementResult.wrapped,
        }));

        return Success(Complete(), log);

    }

    /// {@template BpriverDebug._getPubspecVersion}
    /// pubspec.yaml に version の記載が無い場合 空文字を返す.<br>
    /// {@endtemplate}
    FutureDanger<String, BpriverDebugExceptionBD> _getPubspecVersion(String pubspecFileLocation, [Encoding encoding = utf8]) async {

        final log = Log(classLocation: runtimeType, functionLocation: '_getPubspecVersion');

        const pubspecFileName = 'pubspec.yaml';
        
        final pubspecFilePath = p.join(pubspecFileLocation, pubspecFileName);

        final fileModelResult = await FileModel.fromPath(pubspecFilePath);
        log.add(fileModelResult);
        if (fileModelResult is! Success<FileModel, io.FileSystemException>) return Failure(BpriverDebugException.fromFileSystemException(fileModelResult.asException), log);

        final decodeResult = fileModelResult.wrapped.content.decode(encoding);
        log.add(decodeResult);
        if (decodeResult is! Success<StringContent, FormatException>) return Failure(BpriverDebugException.fromFormatException(decodeResult.asException), log);

        final pubspec = Pubspec.parse(decodeResult.wrapped.value);

        final version = pubspec.version;

        if (version == null) return Success('', log);

        return Success(version.toString(), log);

    }

    // test code で利用されることが前提なので 処理が失敗したとき(exception)は 処理を止める throw error(panic) 必要がある.

    /// {@macro BpriverDebug._getBpriverDebugFolder}
    io.Directory getBpriverDebugFolder() => switch (_getBpriverDebugFolder()) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

    /// {@macro BpriverDebug._getTestFolderPath}
    io.Directory getTestLocationFolder() => switch (_getTestFolderPath()) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

    /// {@macro BpriverDebug._cleanUpTestLocationFolder}
    Complete cleanUpTestLocationFolder() => switch (_cleanUpTestLocationFolder()) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

    /// {@macro BpriverDebug._winMerge}
    Future<Complete> winMerge(Object actual, Object expected) async => switch (await _winMerge(actual, expected)) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

    /// {@macro BpriverDebug._writeLoggerResult}
    Future<Complete> writeLoggerResult(Result loggerResult) async => switch (await _writeLoggerResult(loggerResult)) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

    /// {@macro BpriverDebug._createFileSystem}
    Future<Complete> createFileSystem(Iterable<FileSystemModel> fileSystems) async => switch (await _createFileSystem(fileSystems)) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

    /// {@macro BpriverDebug._getPubspecVersion}
    Future<String> getPubspecVersion(String pubspecFileLocation, [Encoding encoding = utf8]) async => switch (await _getPubspecVersion(pubspecFileLocation, encoding)) {
        Success(wrapped: final expected) => expected,
        Failure(wrapped: final exception, log: final log) => Result.panic(BpriverDebugErrorA(), log.monitor({'exception': exception})),
    };

}
