// Copyright (C) 2023, the bpriver_debug project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_debug/src/foundation/foundation_layer.dart';

/// {@template BpriverDebugError}
/// {@endtemplate}
sealed class BpriverDebugError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage,
        };
    }

}

/// {@template BpriverDebugErrorA}
/// Exceptions occur.
/// {@endtemplate}
class BpriverDebugErrorA
    extends
        BpriverDebugError
{

    static const LOGGER_RESULT_MESSAGE = [
        'Exceptions occur.',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    BpriverDebugErrorA();

}