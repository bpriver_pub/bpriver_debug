// Copyright (C) 2023, the bpriver_debug project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_debug/src/foundation/foundation_layer.dart';

/// {@template BpriverDebugException}
/// {@endtemplate}
sealed class BpriverDebugException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const BpriverDebugException();

    static BpriverDebugExceptionB fromFileSystemException(io.FileSystemException exception) => BpriverDebugExceptionB();
    static BpriverDebugExceptionB fromFileSystemModelExceptionD(FileSystemModelExceptionD exception) => BpriverDebugExceptionB();
    static BpriverDebugExceptionC fromRosterPatternExceptionA(RosterPatternExceptionA exception) => BpriverDebugExceptionC();
    static BpriverDebugExceptionD fromFormatException(FormatException exception) => BpriverDebugExceptionD();
    static BpriverDebugExceptionE fromBpriverYamlException(BpriverYamlException exception) => BpriverDebugExceptionE();

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage,
        };
    }

}

/// {@template BpriverDebugExceptionA}
/// current folder の直下に .dart_tool folder が存在しません.<br>
/// current folder を移動する, もしくは, dart pub get を実行し .dart_tool folder を作成してください.<br>
/// {@endtemplate}
class BpriverDebugExceptionA
    extends
        BpriverDebugException
    implements
        BpriverDebugExceptionAB
        ,BpriverDebugExceptionABC
        ,BpriverDebugExceptionABE
{

    static const LOGGER_RESULT_MESSAGE = [
        'current folder の直下に .dart_tool folder が存在しません.',
        'current folder を移動する, もしくは, dart pub get を実行し .dart_tool folder を作成してください.',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    BpriverDebugExceptionA();

}

/// {@template BpriverDebugExceptionB}
/// copied from [io.FileSystemException]<br>
/// Exception thrown when a file operation fails.
/// {@endtemplate}
final class BpriverDebugExceptionB
    extends
        BpriverDebugException
    implements
        BpriverDebugExceptionAB
        ,BpriverDebugExceptionABC
        ,BpriverDebugExceptionBD
        ,BpriverDebugExceptionABE
{

    static const LOGGER_RESULT_MESSAGE = [
        'Exception thrown when a file operation fails',
    ];
    
    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverDebugExceptionB}
    BpriverDebugExceptionB();

}

/// {@template BpriverDebugExceptionC}
/// [RosterPatternExceptionA].<br>
/// exists duplicate primary key.<br>
/// {@endtemplate}
final class BpriverDebugExceptionC
    extends
        BpriverDebugException
    implements
        BpriverDebugExceptionABC
{

    static const LOGGER_RESULT_MESSAGE = RosterPatternExceptionA.LOGGER_RESULT_MESSAGE;

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverDebugExceptionC}
    const BpriverDebugExceptionC();

}

/// {@template BpriverDebugExceptionD}
/// [FormatException].<br>
/// Exception thrown when a string or some other data does not have an expected format and cannot be parsed or processed.
/// {@endtemplate}
final class BpriverDebugExceptionD
    extends
        BpriverDebugException
    implements
        BpriverDebugExceptionBD
{

    static const LOGGER_RESULT_MESSAGE = [
        '[FormatException].',
        'Exception thrown when a string or some other data does not have an expected format and cannot be parsed or processed.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverDebugExceptionD}
    const BpriverDebugExceptionD();

}

/// {@template BpriverDebugExceptionE}
/// [BpriverYamlException].<br>
/// {@endtemplate}
final class BpriverDebugExceptionE
    extends
        BpriverDebugException
    implements
        BpriverDebugExceptionABE
{

    static const LOGGER_RESULT_MESSAGE = [
        '[BpriverYamlException].',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverDebugExceptionE}
    const BpriverDebugExceptionE();

}

/// * [BpriverDebugExceptionA]
/// {@macro BpriverDebugExceptionA}
/// * [BpriverDebugExceptionB]
/// {@macro BpriverDebugExceptionB}
sealed class BpriverDebugExceptionAB
    extends
        BpriverDebugException
    implements
        BpriverDebugExceptionABC
        ,BpriverDebugExceptionABE
{}

/// * [BpriverDebugExceptionA]
/// {@macro BpriverDebugExceptionA}
/// * [BpriverDebugExceptionB]
/// {@macro BpriverDebugExceptionB}
/// * [BpriverDebugExceptionC]
/// {@macro BpriverDebugExceptionC}
sealed class BpriverDebugExceptionABC
    extends
        BpriverDebugException
{}

/// * [BpriverDebugExceptionB]
/// {@macro BpriverDebugExceptionB}
/// * [BpriverDebugExceptionD]
/// {@macro BpriverDebugExceptionD}
sealed class BpriverDebugExceptionBD
    extends
        BpriverDebugException
{}

/// * [BpriverDebugExceptionA]
/// {@macro BpriverDebugExceptionA}
/// * [BpriverDebugExceptionB]
/// {@macro BpriverDebugExceptionB}
/// * [BpriverDebugExceptionE]
/// {@macro BpriverDebugExceptionE}
sealed class BpriverDebugExceptionABE
    extends
        BpriverDebugException
{}
