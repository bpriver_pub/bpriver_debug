// Copyright (C) 2023, the bpriver_debug project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library foundation_layer;

import 'dart:io' as io;

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';

part 'bpriver_debug_error/bpriver_debug_error.dart';
part 'bpriver_debug_exception/bpriver_debug_exception.dart';
