// Copyright (C) 2023, the bpriver_debug project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library bpriver_debug;

import 'dart:convert';
import 'dart:io' as io;

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:file_system_model/file_system_model.dart';
import 'package:path/path.dart' as p;
import 'package:pubspec_parse/pubspec_parse.dart';

import 'package:bpriver_debug/src/foundation/foundation_layer.dart';

export 'src/foundation/foundation_layer.dart';

part 'src/bpriver_debug/bpriver_debug.dart';
